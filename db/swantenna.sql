CREATE TABLE entry (
  category_id BIGINT UNSIGNED NOT NULL,
  entry_id BIGINT UNSIGNED NOT NULL,
  blog_id BIGINT UNSIGNED NOT NULL,
  remote_name VARCHAR(521),
  local_name VARCHAR(521),
  author VARCHAR(255),
  timestamp DATETIME NOT NULL DEFAULT 0,
  title VARCHAR(1023),
  body BLOB,
  files BLOB NOT NULL,
  PRIMARY KEY (entry_id),
  UNIQUE KEY (blog_id, remote_name),
  UNIQUE KEY (blog_id, local_name),
  KEY (timestamp),
  KEY (category_id, timestamp),
  KEY (blog_id, timestamp)
) DEFAULT CHARSET=BINARY Engine=InnoDB;

CREATE TABLE blog (
  category_id BIGINT UNSIGNED NOT NULL,
  blog_id BIGINT UNSIGNED NOT NULL,
  name VARCHAR(255) NOT NULL,
  title VARCHAR(1023) NOT NULL,
  repository_type TINYINT UNSIGNED NOT NULL,
  repository_url VARCHAR(521),
  viewer_url VARCHAR(521),
  viewer_type TINYINT UNSIGNED NOT NULL,
  icon_url VARCHAR(521),
  created DATETIME NOT NULL DEFAULT 0,
  PRIMARY KEY (blog_id),
  UNIQUE KEY (name),
  KEY (category_id, created),
  KEY (repository_url),
  KEY (created)
) DEFAULT CHARSET=BINARY Engine=InnoDB;
