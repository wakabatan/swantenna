#!/bin/sh
exec 2>&1
export LANG=C
export TZ=UTC
export KARASUMA_CONFIG_JSON=@@ROOT@@/config/@@ENV@@.json
export KARASUMA_CONFIG_FILE_DIR_NAME=@@ROOT@@/local/keys/@@ENV@@
export MYSQL_DSNS_JSON=@@LOCAL@@/keys/@@ENV@@/dsns.json
export WEBUA_DEBUG=`@@ROOT@@/perl @@ROOT@@/modules/karasuma-config/bin/get-json-config.pl env_WEBUA_DEBUG text`
export SQL_DEBUG=`@@ROOT@@/perl @@ROOT@@/modules/karasuma-config/bin/get-json-config.pl env_SQL_DEBUG text`
port=`@@ROOT@@/perl @@ROOT@@/modules/karasuma-config/bin/get-json-config.pl web_port text`

eval "exec setuidgid @@USER@@ @@ROOT@@/plackup \
    $PLACK_COMMAND_LINE_ARGS \
    --host 127.0.0.1 -p $port @@ROOT@@/bin/server.psgi"
