#!/bin/sh
basedir=$(cd `dirname $0`/.. && pwd)
reposdir=${SWA_UPDATER_REPOS_DIR-xxx}
reponame=${SWA_UPDATER_REPO_NAME-xxx}
repourl=${SWA_UPDATER_REPO_URL}
githg=$basedir/modules/git-hg/bin/git-hg

repodir=$reposdir/$reponame
mkdir -p $repodir

exec > $repodir/update.log 2>&1
date
mkdir -p $repodir/index

$githg clone ${repourl} $repodir/tree

cd $repodir/tree && \
    $githg pull && \
    git log --format=raw --raw > ../index/gitlog.txt && \
    $basedir/perl $basedir/modules/vcutils/bin/gitlog2json.pl \
        $repodir/index/gitlog.txt > $repodir/index/gitlog.json && \
    $basedir/perl $basedir/modules/vcutils/bin/generate-hg2gitmap.pl > $repodir/index/git2hg.json

date
