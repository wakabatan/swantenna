use strict;
use warnings;
use Path::Tiny;
use JSON::PS;
use Web::UserAgent::Functions qw(http_get);
use SWA::Defs::Repositories;
use SWA::MySQL;
use SWA::Action::ParseFeed;
use SWA::Action::ParseBugzilla;
use SWA::Action::ImportRepositoryData;

my $VERBOSE = $ENV{VERBOSE};

SWA::MySQL->load_by_envs;

my $root_path = path (__FILE__)->parent->parent->absolute;
my $repos_path = $root_path->child ('local/repos');
my $sources_path = path (shift or die "Usage: $0 sources.json\n")->absolute;
my $sources = json_bytes2perl $sources_path->slurp;

local $ENV{SWA_UPDATER_REPOS_DIR} = $repos_path->stringify;
for my $config (@{$sources->{sources}}) {
  my $name = $config->{name} or die "No |name|";
  warn "$name...\n" if $VERBOSE;
  local $ENV{SWA_UPDATER_REPO_NAME} = $name;
  local $ENV{SWA_UPDATER_REPO_URL} = $config->{repository_url};
  my $repo_path = $repos_path->child ($name);

  # XXX error handling

  my $sh_file_name = $SWA::Defs::Repositories::TypeString2RepoUpdaterName->{$config->{repository_type}};
  if (defined $sh_file_name) {
    my $sh_path = $root_path->child ('bin', $sh_file_name);
    (system 'sh', $sh_path) == 0 or die "$sh_path: $?";
  } elsif ($config->{repository_type} eq 'bugzilla') {
    my $parse_action = SWA::Action::ParseBugzilla->new_from_repository_path_and_info
        ($repo_path, $config);
    $parse_action->set_date;

    for (@{$config->{products_and_components} or []}) {
      $parse_action->save_by_product_and_component_as_cv ($_->[0], $_->[1])->recv;
    }
  } elsif ($config->{repository_type} =~ /^(?:atom|rss)-/) {
    my $url = $config->{repository_url};
    my ($req, $res) = http_get url => $url, timeout => 600;
    unless ($res->is_success) {
      warn "Can't get <$url>\n";
      next;
    }

    my $parse_action = SWA::Action::ParseFeed->new_from_repository_path_and_type
        ($repo_path, $config->{repository_type});
    $parse_action->parse_and_write_stringref (\ $res->content);
  } else {
    warn "Unknown repository type <$config->{repository_type}>";
    next;
  }

  my $action = SWA::Action::ImportRepositoryData->new_from_repository_path_and_info
      ($repo_path, $config);
  $action->import_data;
}

=head1 LICENSE

Copyright 2012-2015 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
