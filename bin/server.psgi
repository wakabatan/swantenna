use strict;
use warnings;
use SWA::Web;
use SWA::Warabe::App;
use Wanage::HTTP;
use SWA::MySQL;

SWA::MySQL->load_by_envs;

sub ($) {
  my $http = Wanage::HTTP->new_from_psgi_env ($_[0]);
  return $http->send_response (onready => sub () {
    my $app = SWA::Warabe::App->new_from_http ($http);
    $app->execute (sub () {
      return SWA::Web->process ($app);
    });
  });
};

=head1 LICENSE

Copyright 2012-2014 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
