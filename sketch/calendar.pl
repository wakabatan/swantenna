use strict;
use warnings;
use Path::Class;
use lib file (__FILE__)->dir->parent->subdir ('lib')->stringify;
use lib glob file (__FILE__)->dir->parent->subdir ('modules', '*', 'lib')->stringify;
use SWA::Object::Calendar;
use Data::Dumper;
use Time::Local qw(timegm_nocheck);

my ($y, $m) = @ARGV;
my $time = timegm_nocheck 0, 0, 0, 1, $m - 1, $y;

my $cal = SWA::Object::Calendar->new_from_date ($time);
warn Dumper $cal->day_list;

