use strict;
use warnings;
use Path::Class;
use lib file (__FILE__)->dir->parent->subdir ('lib')->stringify;
use lib glob file (__FILE__)->dir->parent->subdir ('modules', '*', 'lib')->stringify;
use SWA::Action::ParseAtomFeed;
use Web::UserAgent::Functions qw(http_get);

my $url = shift or die "Usage: $0 url.atom";

my ($req, $res) = http_get url => $url;

my $root_d = file (__FILE__)->dir->parent->subdir ('tmp', 'repo');
my $action = SWA::Action::ParseAtomFeed->new_from_repo_d ($root_d);

#my $entries = $action->parse_byte_stringref (\ $res->content);
#use Data::Dumper;
#warn Dumper $entries;

$action->parse_and_write_stringref (\ $res->content);
