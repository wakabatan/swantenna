package SWA::Warabe::App;
use strict;
use warnings;
use Warabe::App;
push our @ISA, qw(Warabe::App);
use Temma;
use Path::Class;

my $templates_d = file (__FILE__)->dir->parent->parent->parent
    ->subdir ('templates');

sub process_temma ($$$$) {
  my ($app, $template_path, $args) = @_;
  my $http = $app->http;
  $http->response_mime_type->set_value ('text/html');
  $http->response_mime_type->set_param (charset => 'utf-8');
  my $fh = SWA::Warabe::App::TemmaPrinter->new_from_http ($http);
  $args->{app} = $app;
  Temma->process_html
      ($templates_d->file (@$template_path), $args => $fh,
       sub { undef $fh; $http->close_response_body });
} # process_temma

package SWA::Warabe::App::TemmaPrinter;

sub new_from_http ($$) {
  return bless {http => $_[1], value => ''}, $_[0];
} # new_from_http

sub print ($$) {
  $_[0]->{value} .= $_[1];
  if (length $_[0]->{value} > 1024*10 or length $_[1] == 0) {
    $_[0]->{http}->send_response_body_as_text ($_[0]->{value});
    $_[0]->{value} = '';
  }
} # print

sub DESTROY {
  $_[0]->{http}->send_response_body_as_text ($_[0]->{value})
      if length $_[0]->{value};
} # DESTROY

1;

=head1 LICENSE

Copyright 2014 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
