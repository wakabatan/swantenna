package SWA::Action::ImportRepositoryData;
use strict;
use warnings;
use Dongry::Database;
use JSON::PS qw(file2perl json_bytes2perl);
use SWA::Defs::Categories;
use SWA::Defs::Repositories;

sub new_from_repository_path_and_info ($$$) {
  return bless {repository_path => $_[1], repository_info => $_[2]}, $_[0];
} # new_from_repository_path

sub repository_path ($) {
  return $_[0]->{repository_path};
} # repository_path

sub repository_info ($) {
  return $_[0]->{repository_info} || {};
} # repository_info

sub import_data ($) {
  my $self = shift;
  my $info = $self->repository_info;
  return unless $info->{name};

  my $category_id = $SWA::Defs::Categories::NameToCategoryID->{$info->{category}} || 0;

  my $db = Dongry::Database->load ('swantenna');
  my $blog_data = {
    category_id => $category_id,
    title => $info->{title},
    repository_type => $SWA::Defs::Repositories::TypeString2Number->{$info->{repository_type}},
    repository_url => $info->{repository_url},
    viewer_url => $info->{viewer_url},
    viewer_type => $SWA::Defs::Repositories::ViewerTypeString2Number->{$info->{viewer_type} || ''} || 0,
    icon_url => $info->{icon_url},
  };
  $db->table ('blog')->insert ([{
    blog_id => $db->bare_sql_fragment ('uuid_short ()'),
    name => $info->{name},
    %$blog_data,
  }], duplicate => $blog_data);
  my $blog_id = $db->select
      ('blog', {name => $info->{name}}, fields => 'blog_id')->first->{blog_id}
          or die "Can't retrieve blog row with name |$info->{name}|";

  my $insert = [];
  if ($info->{repository_type} =~ /^(?:git|hg|svn|git-svn)$/) {
    $insert = $self->_import_data_git ($info, $blog_id);
  } elsif ($info->{repository_type} =~ /atom|rss/) {
    $insert = $self->_import_data_feed ($info, $blog_id);
  } elsif ($info->{repository_type} eq 'bugzilla') {
    $insert = $self->_import_data_bugzilla ($info, $blog_id);
  }
  for (@$insert) {
    $_->{entry_id} = $db->bare_sql_fragment ('UUID_SHORT()');
    $_->{category_id} = $category_id;
    $_->{body} = substr $_->{body}, 0, 10*1024
        if defined $_->{body} and 10*1024 < length $_->{body};
  }
  while (@$insert) {
    my @ins;
    push @ins, @$insert ? shift @$insert : () for 1..10;
    $db->table ('entry')->insert
        ([grep { $_ } @ins], duplicate => 'replace');
  }
} # import_data

sub _import_data_git ($$$) {
  my ($self, $info, $blog_id) = @_;

  my $repository_path = $self->repository_path;

  my $index_f = $repository_path->child ('index', 'gitlog.json');
  my $gitlog = file2perl $index_f;

  my $get_remote_revision = sub { return undef };
  if ($info->{repository_type} eq 'hg') {
    my $map_f = $repository_path->child ('index', 'git2hg.json');
    my $map = file2perl $map_f;
    $get_remote_revision = sub { return $map->{git2hg}->{$_[0]->{commit}} };
  } elsif ($info->{repository_type} eq 'svn') {
    $get_remote_revision = sub { return $_[0]->{svn_revision} };
  } elsif ($info->{repository_type} eq 'git') {
    $get_remote_revision = sub { return $_[0]->{commit} };
  } elsif ($info->{repository_type} eq 'git-svn') {
    $get_remote_revision = sub {
      if ($_[0]->{body} =~ /(?:\n+|^)git-svn-id: \S+?\@([0-9]+)/) {
        return $1;
      } else {
        return undef;
      }
    };
  } else {
    return;
  }

  my @insert;
  for my $commit (@{$gitlog->{commits} or []}) {
    $commit->{author}->{name} = {
      ianh => 'Ian Hickson',
      howcom => 'Håkon Wium Lie',
    }->{$commit->{author}->{name}} || $commit->{author}->{name};
    my $entry_data = {
      blog_id => $blog_id,
      local_name => $commit->{commit},
      remote_name => $get_remote_revision->($commit),
      body => $commit->{body},
      author => $commit->{author}->{name},
      timestamp => $commit->{committer}->{time} || $commit->{author}->{time},
      files => $commit->{files} || {},
    };
    push @insert, $entry_data;
  }
  return \@insert;
} # _import_data_git

sub _import_data_feed ($$$) {
  my ($self, $info, $blog_id) = @_;

  my $repository_path = $self->repository_path;

  my @insert;
  return [] unless $repository_path->child ('entries')->is_dir;
  for my $d ($repository_path->child ('entries')->children) {
    next unless -d $d;
    next if $d =~ m{/1970-01$};
    for my $f ($d->children) {
      next unless -f $f;
      next unless $f =~ /\.json$/;
      
      my $entry = file2perl $f;
      push @insert, {
        blog_id => $blog_id,
        local_name => $entry->{sha},
        remote_name => $entry->{id},
        title => $entry->{title},
        body => $entry->{body},
        author => $entry->{author_name},
        timestamp => $entry->{updated} || 0,
        files => {},
      };
    }
  }
  return \@insert;
} # _import_data_feed

sub _import_data_bugzilla ($$$) {
  my ($self, $info, $blog_id) = @_;

  my $repository_path = $self->repository_path;

  my @insert;
  return [] unless $repository_path->child ('bugs')->is_dir;
  for my $path ($repository_path->child ('bugs')->children (qr/\.json$/)) {
    my $data = json_bytes2perl $path->slurp;
    for my $id (keys %{$data->{long_desc} or {}}) {
      my $d = $data->{long_desc}->{$id};
      push @insert, {
        blog_id => $blog_id,
        local_name => $id,
        remote_name => $data->{bug_id} . '#c' . $d->{comment_count},
        title => $data->{short_desc} // '',
        body => $d->{thetext} // '',
        author => $d->{who} // '',
        timestamp => $d->{bug_when} || 0,
        files => {
          props => {
            bug_status => $data->{bug_status},
            resolution => $data->{resolution},
            component => $data->{component},
            product => $data->{product},
          },
        },
      };
    }
  }
  return \@insert;
} # _import_data_bugzilla

1;

=head1 LICENSE

Copyright 2012-2014 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
