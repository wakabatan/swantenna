package SWA::Action::ParseBugzilla;
use strict;
use warnings;
use AnyEvent;
use JSON::PS;
use SWA::Bugzilla;

sub new_from_repository_path_and_info ($$$) {
  return bless {repository_path => $_[1], info => $_[2]}, $_[0];
} # new_from_repository_path_and_info

sub repository_path ($) {
  return $_[0]->{repository_path};
} # repository_path

sub repository_info ($) {
  return $_[0]->{info};
} # repository_info

sub _bz ($) {
  $_[0]->{_bz} ||= SWA::Bugzilla->new_from_root_url
      ($_[0]->repository_info->{repository_url});
} # _bz

sub from_date ($) {
  return $_[0]->{from_date} || die "|from_date| not set";
} # from_date

sub to_date ($) {
  return $_[0]->{to_date} || die "|to_date| not set";
} # to_date

sub set_date ($) {
  my $self = $_[0];
  my $to = time;
  my $from = $to - 2*24*60*60;
  $to = [gmtime $to];
  $from = [gmtime $from];
  $self->{from_date} = sprintf '%04d-%02d-%02d',
      $from->[5]+1900, $from->[4]+1, $from->[3];
  $self->{to_date} = sprintf '%04d-%02d-%02d',
      $to->[5]+1900, $to->[4]+1, $to->[3];
} # set_date

sub save_by_product_and_component_as_cv ($$$) {
  my ($self, $product, $component) = @_;
  my $cv = AE::cv;
  my $bugzilla = $self->_bz;
  $bugzilla->get_bug_ids_as_cv
      (product => $product, component => $component,
       from_date => $self->from_date,
       to_date => $self->to_date)->cb (sub {
    my $ids = $_[0]->recv;
    $bugzilla->get_bugs_by_ids_as_cv ($ids)->cb (sub {
      my $data = $_[0]->recv;
      for my $id (keys %$data) {
        $self->_save ($id => $data->{$id});
      }
      $cv->send;
    });
  });
  return $cv;
} # save_by_product_and_component_as_cv

sub _save ($$$) {
  my ($self, $bug_id, $data) = @_;
  my $path = $self->repository_path->child ('bugs', $bug_id . '.json');
  $path->parent->mkpath;
  $path->spew (perl2json_bytes_for_record $data);
} # _save

1;
