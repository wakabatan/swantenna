package SWA::Action::ParseFeed;
use strict;
use warnings;
use Encode;
use Digest::SHA1 qw(sha1_hex);
use JSON::PS;
use Web::DOM::Document;
use Web::XML::Parser;

sub new_from_repository_path_and_type ($$$) {
  return bless {repository_path => $_[1], repository_type => $_[2]}, $_[0];
} # new_from_repository_path

sub repository_path ($) {
  return $_[0]->{repository_path};
} # repository_path

sub repository_type ($) {
  return $_[0]->{repository_type};
} # repository_type

sub parse_byte_stringref {
  my ($self, $sref) = @_;

  my $doc = new Web::DOM::Document;
  my $parser = new Web::XML::Parser;
  $parser->onerror (sub { });
  $parser->parse_char_string ((decode 'utf-8', $$sref) => $doc);

  return $self->parse_doc ($doc);
} # parse_byte_stringref

sub parse_doc ($$) {
  my ($self, $doc) = @_;
  my $type = $self->repository_type;
  if ($type =~ /^atom-/) {
    return $self->parse_atom_doc ($doc);
  } elsif ($type =~ /^rss-/) {
    return $self->parse_rss_doc ($doc);
  } else {
    return [];
  }
} # parse_doc

sub parse_atom_doc ($$) {
  my ($self, $doc) = @_;

  my $entries = [];

  my $feed_el = $doc->atom_feed_element or return;
  for my $entry_el (@{$feed_el->entry_elements}) {
    my $entry = {};

    my $title_el = $entry_el->title_element;
    $entry->{title} = $title_el->container->text_content if $title_el;

    my $author_el = $entry_el->author_elements->[0];
    if ($author_el) {
      $entry->{author_name} = $author_el->name;
      $entry->{author_email} = $author_el->email;
    }

    my $link_el = [grep { $_->rel eq q<http://www.iana.org/assignments/relation/alternate> and not defined $_->type } @{$entry_el->link_elements}]->[0];
    $entry->{url} = $link_el->href if $link_el;

    $entry->{id} = $entry_el->atom_id;

    my $updated_el = $entry_el->updated_element;
    if (defined $updated_el) {
      $entry->{updated} = $updated_el->value;
      unless ($entry->{updated}) {
        if ($updated_el->text_content =~ /([0-9]+-[0-9]+-[0-9]+T[0-9]+:[0-9]+:[0-9]+)/) {
          $updated_el->text_content ("$1Z");
          $entry->{updated} = $updated_el->value;
        }
      }
    }

    my $content_el = $entry_el->content_element;
    $entry->{body} = $content_el->container->text_content if $content_el;
    if (defined $entry->{body}) {
      $entry->{body} =~ s/\A\x0A+//;
      $entry->{body} =~ s/\x0A+\z//;
    }

    push @$entries, $entry;
  }

  return $entries;
} # parse_atom_doc

sub parse_rss_doc ($$) {
  my ($self, $doc) = @_;

  my $entries = [];

  my $html_doc = new Web::DOM::Document;
  $html_doc->manakai_is_html (1);
  my $div_el = $html_doc->create_element ('div');
  my $updated_el = $doc->create_element_ns ('http://www.w3.org/2005/Atom', 'updated');
  for my $entry_el (@{$doc->query_selector_all ('RDF > item')}) {
    my $entry = {};

    for my $el ($entry_el->children->to_list) {
      my $ln = $el->local_name;
      if ($ln eq 'title') {
        $entry->{title} = $el->text_content;
      } elsif ($ln eq 'link') {
        $entry->{id} = $entry->{url} = $el->text_content;
      } elsif ($ln eq 'date') { # dc:date
        $updated_el->text_content ($el->text_content);
        $entry->{updated} = $updated_el->value;
      } elsif ($ln eq 'description') {
        $entry->{body} = $el->text_content;
      } elsif ($ln eq 'encoded') { # content:encoded
        $entry->{body} //= do {
          $div_el->inner_html ($el->text_content);
          $div_el->text_content;
        };
      }

      #$entry->{author_name} = $author_el->name;
      #$entry->{author_email} = $author_el->email;
    } # $el

    if (defined $entry->{body}) {
      $entry->{body} =~ s/\A\x0A+//;
      $entry->{body} =~ s/\x0A+\z//;
    }

    push @$entries, $entry if defined $entry->{id};
  }

  return $entries;
} # parse_rss_doc

sub get_entry_signature ($$) {
  my ($self, $entry) = @_;
  return sha1_hex encode 'utf-8', join $;, map { defined $_ ? $_ : '' } $entry->{author_name}, $entry->{title}, $entry->{body};
} # get_entry_signature

sub get_entry_path ($$) {
  my ($self, $entry) = @_;

  my (undef, undef, undef, undef, $month, $year) = gmtime ($entry->{updated} || 0);
  $month++;
  $year += 1900;

  return $self->repository_path->child ('entries', sprintf '%04d-%02d', $year, $month)->child ($self->get_entry_signature ($entry) . '.json');
} # get_entry_path

sub parse_and_write_stringref ($$) {
  my $self = shift;

  my $entries = $self->parse_byte_stringref ($_[0]);
  for my $entry (@$entries) {
    $entry->{sha} = $self->get_entry_signature ($entry);
    my $path = $self->get_entry_path ($entry);
    $path->parent->mkpath;
    print { $path->openw } perl2json_bytes_for_record $entry;
  }
} # parse_and_write_stringref

1;

=head1 LICENSE

Copyright 2012-2014 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
