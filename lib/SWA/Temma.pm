package SWA::Temma;
use strict;
use warnings;
use Temma::Parser;
use Temma::Processor;
use Web::DOM::Document;
use Path::Class;

my $template_d = file (__FILE__)->dir->parent->parent->subdir ('templates');

sub new_from_http ($$) {
  return bless {http => $_[1]}, $_[0];
} # new_from_http

sub process ($$;$) {
  my ($self, $file_name, $args) = @_;
  my $template_f = $template_d->file ($file_name);
  return 0 unless -f $template_f;
  my $doc = new Web::DOM::Document;
  Temma::Parser->new->parse_char_string
      (scalar $template_f->slurp => $doc);
  my $fh = SWA::Temma::FH->new_from_http ($self->{http});
  Temma::Processor->new->process_document ($doc => $fh, args => $args);
  return 1;
} # process

package SWA::Temma::FH;

sub new_from_http ($$) {
  return bless {http => $_[1], value => ''}, $_[0];
} # new_from_http

sub print ($$) {
  $_[0]->{value} .= $_[1];
  if (length $_[0]->{value} > 1024*4 or length $_[1] == 0) {
    $_[0]->{http}->send_response_body_as_text ($_[0]->{value});
    $_[0]->{value} = '';
  }
} # print

sub DESTROY {
  $_[0]->{http}->send_response_body_as_text ($_[0]->{value});
}

1;
