package SWA::Defs::Categories;
use strict;
use warnings;
use Exporter::Lite;

our @EXPORT;

push @EXPORT, qw(CATEGORY_TEST CATEGORY_WEB CATEGORY_HOUSES);
sub CATEGORY_TEST () { 0 }
sub CATEGORY_WEB () { 1 }
sub CATEGORY_HOUSES () { 2 }

our $NameToCategoryID = {
  test => CATEGORY_TEST,
  web => CATEGORY_WEB,
  houses => CATEGORY_HOUSES,
};

our $CategoryIDToName = {reverse %$NameToCategoryID};

our $CategoryIDToTitle = {
  (CATEGORY_TEST) => 'Test',
  (CATEGORY_WEB) => 'Web',
  (CATEGORY_HOUSES) => 'Houses',
};

1;

=head1 LICENSE

Copyright 2014-2015 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
