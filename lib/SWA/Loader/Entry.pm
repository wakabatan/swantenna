package SWA::Loader::Entry;
use strict;
use warnings;
use Dongry::Database;
use SWA::MySQL;
use SWA::Object::Entry;

sub new ($) {
  return bless {}, $_[0];
} # new

sub find_entry_by_id ($$) {
  my $self = shift;
  my $id = shift or return undef;
  
  my $db = Dongry::Database->load ('swantenna');
  my $row = $db->table ('entry')->find({entry_id => $id}) or return undef;

  return SWA::Object::Entry->new_from_row ($row);
} # find_entry_by_id

1;
