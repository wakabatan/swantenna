package SWA::Loader::EntryList;
use strict;
use warnings;
use Dongry::Database;
use SWA::Defs::Categories;
use SWA::MySQL;
use SWA::Object::Entry;
use SWA::Object::Blog;

sub new_from_category_id_and_date ($$$) {
  return bless {category_id => $_[1], date => $_[2]}, $_[0];
} # new_from_category_id_and_date

sub category_id ($) {
  return $_[0]->{category_id};
} # category_id

sub category_name ($) {
  return $SWA::Defs::Categories::CategoryIDToName->{$_[0]->category_id} // $_[0]->category_id;
} # category_name

sub category_title ($) {
  return $SWA::Defs::Categories::CategoryIDToTitle->{$_[0]->category_id} // $_[0]->category_name;
} # category_title

sub category_path ($) {
  return sprintf '/%s/',
      $_[0]->category_name;
} # category_path

sub get_category_day_path ($$) {
  my $t = [gmtime $_[1]];
  return sprintf '/%s/%04d-%02d-%02d',
      $_[0]->category_name, $t->[5]+1900, $t->[4]+1, $t->[3];
} # get_category_day_path

sub date ($) {
  return $_[0]->{date};
} # date

sub prev_date ($) {
  return $_[0]->{date} - 24*60*60;
} # prev_date

sub next_date ($) {
  return $_[0]->{date} + 24*60*60;
} # next_date

sub _get_day_label ($$) {
  my $t = [gmtime $_[1]];
  return sprintf '%04d-%02d-%02d', $t->[5]+1900, $t->[4]+1, $t->[3];
} # _get_day_label

sub date_as_label ($) {
  return $_[0]->_get_day_label ($_[0]->date);
} # date_as_label

sub items ($) {
  my $self = shift;

  # XXX pager

  my $where = {timestamp => {'<=', time}, category_id => $self->category_id};
  if ($self->{date}) {
    my (undef, undef, undef, $d1, $m1, $y1) = gmtime $self->{date};
    my (undef, undef, undef, $d2, $m2, $y2) = gmtime ($self->{date} + 24*60*60);
    $where->{timestamp} = {'>=', (sprintf '%04d-%02d-%02d', $y1 + 1900, $m1 + 1, $d1),
                           '<', (sprintf '%04d-%02d-%02d', $y2 + 1900, $m2 + 1, $d2)};
  }

  my $db = Dongry::Database->load ('swantenna');
  my $entries = $db->select ('entry', $where,
                             order => ['timestamp' => 'ASC'],
                             limit => 500)
      ->all_as_rows->map (sub { SWA::Object::Entry->new_from_row ($_) });

  my $blog_ids = $entries->map (sub { $_->blog_id })->as_hashref;
  my $blogs = keys %$blog_ids
      ? {$db->table ('blog')->find_all ({blog_id => {-in => [keys %$blog_ids]}})
           ->map (sub { $_->get ('blog_id') => SWA::Object::Blog->new_from_row ($_) })
           ->to_list}
      : {};
  $entries->each (sub {
    $_->blog ($blogs->{$_->blog_id} || SWA::Object::Blog->new_null_blog);
  });

  return $entries;
} # items

1;

=head1 LICENSE

Copyright 2012-2014 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
