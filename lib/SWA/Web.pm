package SWA::Web;
use strict;
use warnings;
use Path::Class;
use Time::Local qw(timegm_nocheck);
use SWA::Defs::Categories;

my $root_d = file (__FILE__)->dir->parent->parent;

sub process ($$) {
  my ($class, $app) = @_;
  $app->requires_valid_content_length;
  $app->requires_mime_type;
  $app->requires_request_method;
  $app->requires_same_origin unless $app->http->request_method_is_safe;

  $app->http->set_response_header
      ('Strict-Transport-Security' => 'max-age=10886400; includeSubDomains; preload');

  my $path = [@{$app->path_segments}];
  if ($path->[0] eq '') {
    $app->process_temma (['index.html'], {});
    return $app->throw;
  } elsif ($path->[0] eq 'styles' and
           $path->[1] =~ /\A([0-9A-Za-z_-]+\.css)\z/ and
           not defined $path->[2]) {
    my $f = $root_d->subdir ('styles')->file ($1);
    if (-f $f) {
      my $http = $app->http;
      $http->response_mime_type->set_value ('text/css');
      $http->response_mime_type->set_param (charset => 'utf-8');
      $http->set_response_last_modified ($f->stat->mtime);
      $http->send_response_body_as_ref (\scalar $f->slurp);
      $http->close_response_body;
      return $app->throw;
    }
  } elsif ($path->[0] eq 'scripts' and
           $path->[1] =~ /\A([0-9A-Za-z_-]+\.js)\z/ and
           not defined $path->[2]) {
    my $f = $root_d->subdir ('scripts')->file ($1);
    if (-f $f) {
      my $http = $app->http;
      $http->response_mime_type->set_value ('text/javascript');
      $http->response_mime_type->set_param (charset => 'utf-8');
      $http->set_response_last_modified ($f->stat->mtime);
      $http->send_response_body_as_ref (\scalar $f->slurp);
      $http->close_response_body;
      return $app->throw;
    }
  }

  my $category_name = shift @$path;
  my $category_id = $SWA::Defs::Categories::NameToCategoryID->{$category_name};
  return $app->throw_error (404) unless defined $category_id;

  if (($path->[0] eq '' and not defined $path->[1] and $path->[0] =~ /(?:)/) or
      ($path->[0] =~ /\A([0-9]{4,})-([0-9]{2})-([0-9]{2})\z/ and not defined $path->[1])) {
    my $date = defined $1 ? timegm_nocheck 0, 0, 0, $3, $2 - 1, $1 : time - 24*60*60;
    if ($category_name eq 'houses' and $date < time - 30*24*60*60) {
      return $app->throw_error (410);
    }

    require SWA::Loader::EntryList;
    my $loader = SWA::Loader::EntryList->new_from_category_id_and_date
        ($category_id, $date);
    require SWA::Object::Calendar;
    my $cal = SWA::Object::Calendar->new_from_date ($date);
    $app->process_temma
        (['entrylist.html'],
         {entry_list_loader => $loader, calendar => $cal});
    return $app->throw;
  }

  return $app->throw_error (404);
} # process

1;

=head1 LICENSE

Copyright 2012-2015 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
