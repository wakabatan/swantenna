package SWA::Object::Calendar;
use strict;
use warnings;
use Time::Local qw(timegm_nocheck);

sub new_from_date ($) {
  return bless {date => [gmtime $_[1]]}, $_[0];
} # new_from_date

sub year ($) {
  return $_[0]->{date}->[5] + 1900;
} # year

sub month ($) {
  return $_[0]->{date}->[4] + 1;
} # month

sub day ($) {
  return $_[0]->{date}->[3];
} # day

sub get_ymd ($$) {
  return sprintf '%04d-%02d-%02d', $_[0]->{date}->[5] + 1900, $_[0]->{date}->[4] + 1, $_[1];
} # get_ymd

sub get_prev_month ($) {
  my @d = ($_[0]->{date}->[5] + 1900, $_[0]->{date}->[4] + 1 - 1, $_[0]->{date}->[3]);
  $d[0]--, $d[1] = 12 if $d[1] == 0;
  return sprintf '%04d-%02d-%02d', @d;
} # get_prev_month

sub get_next_month ($) {
  my @d = ($_[0]->{date}->[5] + 1900, $_[0]->{date}->[4] + 1 + 1, $_[0]->{date}->[3]);
  $d[0]++, $d[1] = 1 if $d[1] == 13;
  return sprintf '%04d-%02d-%02d', @d;
} # get_next_month

sub day_list ($) {
  my $self = shift;

  my $wday = ($self->{date}->[6] - $self->{date}->[3] + 1 + 7*5) % 7;

  my $last = [gmtime timegm_nocheck 0, 0, 0, 0, $self->{date}->[4] + 1, $self->{date}->[5]]->[3];

  my @day = (1..$last);
  unshift @day, undef for 1..$wday;
  my @month;
  while (@day) {
    my $week = [];
    push @$week, shift @day for 1..7;
    push @month, $week;
  }
  return \@month;
} # day_list

1;
