package SWA::Object::Blog;
use strict;
use warnings;
use SWA::Defs::Repositories;

sub new_from_row ($$) {
  return bless {row => $_[1]}, $_[0];
} # new_from_row

sub new_null_blog ($) {
  return bless {}, $_[0];
} # new_null_blog

sub name ($) {
  return $_[0]->{row} ? $_[0]->{row}->get ('name') : undef;
} # name

sub title ($) {
  return $_[0]->{row} ? $_[0]->{row}->get ('title') : undef;
} # title

sub repository_type ($) {
  return $_[0]->{row} ? $_[0]->{row}->get ('repository_type') : 0;
} # repository_type

sub is_mailing_list ($) {
  return $SWA::Defs::Repositories::RepositoryTypeIsMailingList->{$_[0]->repository_type};
} # is_mailing_list

sub is_bts ($) {
  return $SWA::Defs::Repositories::RepositoryTypeIsBTS->{$_[0]->repository_type};
} # is_bts

sub show_social_links ($) {
  return $_[0]->name =~ /^binterest-/;
} # show_social_links

sub get_entry_link_url_as_string ($$) {
  return $_[0]->get_entry_viewer_url ($_[1]);
} # get_entry_link_url_as_string

sub get_entry_viewer_url ($$) {
  my ($self, $entry) = @_;
  if ($self->name eq 'webapps') {
    return 'http://html5.org/r/' . ($entry->remote_name || return undef);
  }

  my $blog_row = $self->{row} or return undef;
  my $viewer_type = $blog_row->get ('viewer_type');
  if ($viewer_type == REPOSITORY_VIEWER_TYPE_UNKNOWN) {
    return $entry->remote_name; # or undef
  }
  my $url = $blog_row->get ('viewer_url') or return undef;
  if ($viewer_type == REPOSITORY_VIEWER_TYPE_MERCURIAL) {
    $url .= 'rev/'
  } elsif ($viewer_type == REPOSITORY_VIEWER_TYPE_BITBUCKET) {
    $url .= 'changeset/';
  } elsif ($viewer_type == REPOSITORY_VIEWER_TYPE_GITHUB) {
    $url .= '/commit/';
  } elsif ($viewer_type == REPOSITORY_VIEWER_TYPE_W3CML) {
    $url = $entry->remote_name; # or undef
    if (defined $url and $url =~ /^mid:/) {
      $url =~ s/^mid://;
      $url = "https://www.w3.org/mid/$url";
      return $url;
    }
  } elsif ($viewer_type == REPOSITORY_VIEWER_TYPE_BUGZILLA) {
    $url .= 'show_bug.cgi?id=';
  }

  $url .= $entry->remote_name or return undef;
  return $url;
} # get_entry_viewer_url

sub icon_url_as_string ($) {
  return $_[0]->{row} ? $_[0]->{row}->get ('icon_url') : undef;
} # icon_url_as_string

1;
