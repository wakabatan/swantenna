package SWA::Object::Entry;
use strict;
use warnings;
use Carp;
use SWA::Defs::Repositories;

sub new_from_row ($$) {
  return bless {row => $_[1]}, $_[0];
} # new_from_row

sub entry_id ($) {
  return $_[0]->{row}->get ('entry_id');
} # entry_id

sub blog ($;$) {
  if (@_ > 1) {
    $_[0]->{blog} = $_[1];
  }
  return $_[0]->{blog} || croak "|blog| not specified";
} # blog

sub blog_id ($) {
  return $_[0]->{row}->get ('blog_id');
} # blog_id

sub is_mail ($) {
  return $_[0]->blog->is_mailing_list;
} # is_mail

sub is_bug ($) {
  return $_[0]->blog->is_bts;
} # is_bug

sub remote_name ($) {
  return $_[0]->{row}->get ('remote_name');
} # remote_name

sub remote_name_short ($) {
  my $self = shift;
  my $name = $self->remote_name;
  return undef unless defined $name;
  if ($SWA::Defs::Repositories::RemoteNameIsSha1Hash->{$self->blog->repository_type}) {
    return substr $name, 0, 12;
  } else {
    return $name;
  }
} # remote_name_short

sub link_url_as_string ($) {
  my $self = shift;
  return $self->{link_url_as_string}
      ||= $self->blog->get_entry_link_url_as_string ($self);
} # link_url_as_string

sub icon_url_as_string ($) {
  return $_[0]->blog->icon_url_as_string;
} # icon_url_as_string

sub author_name ($) {
  return $_[0]->{row}->get ('author');
} # author_name

sub timestamp ($) {
  return $_[0]->{row}->get ('timestamp');
} # timestamp

sub title ($) {
  my $self = shift;
  return $self->{title} if defined $self->{title};

  my $title = $self->{row}->get ('title');
  if (defined $title) {
    1 while $title =~ s/^\s*Re:\s*//;
    while ($title =~ s/^\s*\[([^\[\]]+)\]\s*//) {
      push @{$self->{topics} ||= []}, $1;
    }
    1 while $title =~ s/^\s*Re:\s*//;
  }
  return $self->{title} = $title;
} # title

sub body ($) {
  my $self = shift;
  return $self->{body} if defined $self->{body};
  
  my $body = $self->{row}->get ('body');

  if ($self->is_mail) {
    $body =~ s/^\s*On .+wrote:\s*$//gm;
    $body =~ s/^\s*[^<>]+<[^<>]+>\s*writes:\s*$//gm;
    $body =~ s/^[>|].*$//gm;
    $body =~ s/\n-- \n.+$//s;
    $body =~ s/^\s*\[\d+\]\s*<?[^\s<>]+>?\s*$//gm;
  } elsif ($self->is_bug) {
    $body =~ s/^(?:Specification|Multipage|Complete|Comment|Referrer|User agent|Posted from):.*$//gm;
  } else {
    $body =~ s/(?:\n+|^)git-svn-id: .+$//;

    if ($self->blog->name eq 'webapps') {
      $body =~ s/^\s*\[([^\[\]]*)\]\s*/$self->{webapps_flags} = $1; ''/ge;
      $body =~ s/^\s*\(([0-9]+)\)\s*/$self->{webapps_level} = $1; ''/ge;
      $body =~ s{\s*Affected topics: (.+)$}{$self->{topics} = [split /\s*,\s*/, $1]; ''}ge;
      $body =~ s{\s*Fixing https://www.w3.org/Bugs/Public/show_bug.cgi\?id=([0-9]+)\s*$}{push @{$self->{w3c_bugs} ||= []}, $1; ''}ge;
    } elsif ($self->blog->name eq 'csswg') {
      unshift @{$self->{topics} ||= []}, $1
          while $body =~ s{^\s*\[([^\[\]]+)\]\s*}{};
    }

    $self->{body_links} = [];
    while ($body =~ s{\s*<(https?:[^<>]+)>\s*$|\s+(https?://\S+)\s*$}{}) {
      my $url = $1 || $2;
      my $title = $url;
      if ($title =~ m{^https?://lists.w3.org/Archives/Public/([^/]+/[^/]+/[0-9]+).html$}) {
        $title = $1;
      }
      if ($url =~ m{^https?://www.w3.org/Bugs/Public/show_bug.cgi\?id=([0-9]+)$}) {
        unshift @{$self->{w3c_bugs} ||= []}, $1;
      } else {
        unshift @{$self->{body_links}}, {url => $url, title => $title};
      }
    }
  } # not is mailinglist

  $body =~ s/\x0D\x0A/\x0A/g;
  $body =~ s/\x0D/\x0A/g;
  $body =~ s/^[>|].*$//gm;
  $body =~ s/\A(?:\s*\x0A)+//;
  $body =~ s/(?:\x0A\s*)+\z//;
  $body =~ s/(?:\x0A\s*)+\x0A/\x0A/g;

  return $self->{body} = $body;
} # body

sub body_summary_as_html ($) {
  my $self = $_[0];
  return $self->{body_summary_as_html} if defined $self->{body_summary_as_html};

  my $body = $self->body;

  if ($self->is_mail or $self->is_bug) {
    $body = substr ($body, 0, 150) . '...'
        if 150 < length $body;
  }

  $body =~ s/&/&amp;/g;
  $body =~ s/</&lt;/g;
  $body =~ s/>/&gt;/g;

  $body =~ s{\b([A-Za-z0-9._-]+\(\))}{<code>$1</code>}g;
  $body =~ s{\b([A-Za-z0-9._-]+=(?:""|''))}{<code>$1</code>}g;
  $body =~ s{(::?[A-Za-z0-9._-]+(?:\(\))?)}{<code>$1</code>}g;
  $body =~ s{\s+/([A-Za-z0-9._-]+)/\s+}{ <var>$1</var> }g;
  $body =~ s{\s+'([A-Za-z0-9._-]+)'\s+}{ <code>'$1'</code> }g;
  $body =~ s{(&lt;/?[A-Za-z0-9._-]+&gt;)}{<code>$1</code>}g;
  $body =~ s{(&lt;[A-Za-z0-9._-]+\s+[A-Za-z0-9._-]+(?:=[A-Za-z0-9._-]+)?&gt;)}{<code>$1</code>}g;

  return $self->{body_summary_as_html} = $body;
} # body_summary_as_html

sub topics ($) {
  my $self = $_[0];
  unless ($self->{topics_by_blog}) {
    push @{$self->{topics} ||= []}, $self->blog->name
        if $self->is_mail;
  }
  $self->title;
  $self->body;
  if ($self->is_bug) {
    my $props = ($self->{row}->get ('files') || {})->{props} || {};
    push @{$self->{topics} ||= []}, $props->{component}
        if defined $props->{component};
  }
  unless ($self->{topics_by_blog}) {
    my %found;
    $self->{topics} = [grep { not $found{$_}++ } @{$self->{topics}}];
    $self->{topics_by_blog} = 1;
  }
  return $self->{topics} || [];
} # topics

sub links ($) {
  return $_[0]->{body_links} || [];
} # links

sub webapps_flags ($) { $_[0]->body; return $_[0]->{webapps_flags} }
sub webapps_level ($) { $_[0]->body; return $_[0]->{webapps_level} }

sub w3c_bugs ($) {
  $_[0]->body;
  return $_[0]->{w3c_bugs} || [];
} # w3c_bugs

sub bugzilla_id ($) {
  if ($_[0]->blog->repository_type == REPOSITORY_TYPE_BUGZILLA) {
    return [split /#c/, $_[0]->remote_name]->[0];
  } else {
    return undef;
  }
} # bugzilla_id

sub bugzilla_comment_id ($) {
  if ($_[0]->blog->repository_type == REPOSITORY_TYPE_BUGZILLA) {
    return [split /#c/, $_[0]->remote_name]->[1];
  } else {
    return undef;
  }
} # bugzilla_comment_id

sub is_editorial ($) {
  my $self = shift;
  return $self->{is_editorial} if defined $self->{is_editorial};

  return $self->{is_editorial} = 1 if ($self->webapps_flags || '') =~ /e/;

  my $body = $self->body;
  return $self->{is_editorial} = 1
      if $body =~ /^\s*\(?(?:typos?|editorial.+|fix\s*typo.*|.*typos?|merge|generated\.\s*do\s+not\s+edit!)\)?\s*$/i;

  return $self->{is_editorial} = 0;
} # is_editorial

sub top_directories ($) {
  my $self = shift;
  return $self->{top_directories} if $self->{top_directories};
  require List::Ish;
  my $row = $self->{row} or return $self->{top_directories} = List::Ish->new;
  return $self->{top_directories} = List::Ish
      ->new ([grep { m{/} } keys %{$row->get ('files') || {}}])
      ->map (sub { my $v = $_; $v =~ s{/.*$}{}; $v })
      ->uniq_by (sub { $_[0] eq $_[1] });
} # top_directories

1;
