package SWA::MySQL;
use strict;
use warnings;
use Path::Class;
use JSON::Functions::XS qw(file2perl);
use Dongry::Type::Time;
use Dongry::Type::JSON;

$Dongry::Database::Registry->{swantenna} = {
  sources => {
    master => {writable => 1},
    default => {},
  },
  schema => {
    blog => {
      type => {
        title => 'text',
        created => 'timestamp',
      },
      default => {
        created => sub { time },
      },
    },
    entry => {
      type => {
        author => 'text',
        timestamp => 'timestamp',
        title => 'text',
        body => 'text',
        files => 'json',
      },
      default => {
        timestamp => sub { time },
      },
    },
  },
}; # swantenna

sub load_dsns_from_json ($$) {
  my (undef, $parsed) = @_;
  
  my $dsn = $parsed->{dsns}->{swantenna}
      or die "dsn for |swantenna| not defined";
  $Dongry::Database::Registry->{swantenna}->{sources}->{default}->{dsn} = $dsn;
  $Dongry::Database::Registry->{swantenna}->{sources}->{master}->{dsn} = $dsn;
} # load_dsns_from_json

sub load_by_envs ($) {
  my $dsn_f = file ($ENV{MYSQL_DSNS_JSON} or die "|MYSQL_DSNS_JSON| not specified");
  __PACKAGE__->load_dsns_from_json (file2perl $dsn_f);
} # load_by_envs

1;
