package SWA::Bugzilla;
use strict;
use warnings;
use Encode;
use Time::Local qw(timegm_nocheck);
use AnyEvent;
use Web::XML::Parser;
use Web::DOM::Document;
use Web::UserAgent::Functions qw(http_get);

sub new_from_root_url ($$) {
  return bless {root_url => $_[1]}, $_[0];
} # new_from_root_url

sub get_buglist_url ($%) {
  my ($self, %args) = @_;
  die "No |product|" unless defined $args{product} and length $args{product};
  die "No |component|" unless defined $args{component};
  return sprintf q<https://www.w3.org/Bugs/Public/buglist.cgi?chfieldfrom=%s&chfieldto=%s&component=%s&product=%s&ctype=csv>,
      $args{from_date} // '',
      $args{to_date} // '',
      $args{component}, $args{product};
} # get_buglist_url

sub get_bugs_url ($%) {
  my ($self, %args) = @_;
  die "No |ids|" unless defined $args{ids} and @{$args{ids}};
  return q<https://www.w3.org/Bugs/Public/show_bug.cgi?ctype=xml&>
      . join '&', map { 'id=' . $_ } @{$args{ids}};
} # get_bugs_url

sub get_bug_ids_as_cv ($%) {
  my ($self, %args) = @_;
  my $cv = AE::cv;
  my $url = $self->get_buglist_url (%args);
  http_get
      url => $url,
      cookies => {COLUMNLIST => 'bug_id'},
      anyevent => 1,
      cb => sub {
        my (undef, $res) = @_;
        my %id;
        for (split /\x0D?\x0A/, $res->content) {
          if (/^\"?([0-9]+)\"?/) {
            $id{$1} = 1;
          }
        }
        $cv->send (\%id);
      };
  return $cv;
} # get_bug_ids_as_cv

sub get_bugs_by_ids_as_cv ($$) {
  my ($self, $ids) = @_;
  my $cv = AE::cv;
  my $data = {};
  my @all_id = ref $ids eq 'ARRAY' ? @$ids : keys %$ids;
  my $run; $run = sub {
    my @id = splice @all_id, 0, 10;
    $self->_get_bugs_by_ids_as_cv (\@id => $data)->cb (sub {
      if (@all_id) {
        my $timer; $timer = AE::timer 0, 1, sub {
          $run->();
          undef $timer;
        };
      } else {
        $cv->send ($data);
      }
    });
  };
  if (@all_id) {
    $run->();
  } else {
    AE::postpone { $cv->send ($data) };
  }
  return $cv;
} # get_bugs_by_ids_as_cv

sub _ts ($) {
  if ($_[0] =~ /([0-9]+)-([0-9]+)-([0-9]+)\s+([0-9]+):([0-9]+):([0-9]+)\s+([+-][0-9]{2})([0-9]+)/) {
    return timegm_nocheck $6, $5-$8, $4-$7, $3, $2-1, $1;
  } else {
    return 0;
  }
} # _ts

sub _get_bugs_by_ids_as_cv ($$$) {
  my ($self, $ids => $data) = @_;
  my $cv = AE::cv;
  my $url = $self->get_bugs_url (ids => $ids);
  http_get
      url => $url,
      anyevent => 1,
      cb => sub {
        my (undef, $res) = @_;
        if ($res->is_success) {
          my $doc = new Web::DOM::Document;
          my $parser = new Web::XML::Parser;
          $parser->parse_char_string ((decode 'utf-8', $res->content) => $doc);
          my $de = $doc->document_element;
          if (defined $de) {
            for my $bug_el (@{$de->children}) {
              my $bug = {};
              my $bug_id;
              for my $prop_el (@{$bug_el->children}) {
                my $prop_name = $prop_el->local_name;
                if ($prop_name eq 'bug_id') {
                  $bug_id = $prop_el->text_content;
                  $bug->{$prop_name} = $bug_id;
                } elsif ($prop_name eq 'creation_ts') {
                  $bug->{$prop_name} = _ts $prop_el->text_content;
                } elsif ({
                  product => 1, component => 1,
                  bug_status => 1, resolution => 1,
                  short_desc => 1,
                }->{$prop_name}) {
                  $bug->{$prop_name} = $prop_el->text_content;
                } elsif ($prop_name eq 'long_desc') {
                  my $comment = {};
                  my $comment_id;
                  for my $p_el (@{$prop_el->children}) {
                    my $p_name = $p_el->local_name;
                    if ($p_name eq 'commentid') {
                      $comment_id = $p_el->text_content;
                    } elsif ({
                      comment_count => 1, thetext => 1,
                    }->{$p_name}) {
                      $comment->{$p_name} = $p_el->text_content;
                    } elsif ($p_name eq 'who') {
                      $comment->{$p_name} = $p_el->get_attribute ('name');
                    } elsif ($p_name eq 'bug_when') {
                      $comment->{$p_name} = _ts $p_el->text_content;
                    }
                  } # $p_el
                  $bug->{$prop_name}->{$comment_id} = $comment;
                }
              } # $prop_el
              $data->{$bug_id} = $bug;
            }
          }
        }
        $cv->send;
      };
  return $cv;
} # _get_bugs_by_ids_as_cv

1;
