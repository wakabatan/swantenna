all:

WGET = wget
CURL = curl
GIT = git

updatenightly: local/bin/pmbp.pl
	$(CURL) https://gist.githubusercontent.com/wakaba/34a71d3137a52abb562d/raw/gistfile1.txt | sh
	$(GIT) add modules t_deps/modules
	perl local/bin/pmbp.pl --update
	$(GIT) add config

## ------ Setup ------

APTGET = apt-get

# sudo!
apt-install:
	$(APTGET) install -y git-svn mercurial

deps: git-submodules pmbp-install git-hg-deps

git-submodules:
	$(GIT) submodule update --init

git-hg-deps:
	cd modules/git-hg && $(GIT) submodule update --init

local/bin/pmbp.pl:
	mkdir -p local/bin
	$(WGET) -O $@ https://raw.github.com/wakaba/perl-setupenv/master/bin/pmbp.pl
pmbp-upgrade: local/bin/pmbp.pl
	perl local/bin/pmbp.pl --update-pmbp-pl
pmbp-update: git-submodules pmbp-upgrade
	perl local/bin/pmbp.pl --update
pmbp-install: pmbp-upgrade
	perl local/bin/pmbp.pl --install

## ------ Development ------

PERL = ./perl
PREPARE_DB_SET = modules/rdb-utils/bin/prepare-db-set.pl

develdb-start:
	$(PERL) $(PREPARE_DB_SET) --preparation-file-name db/prep.txt \
            --dsn-list t/tmp/dsns.json

develdb-import:
	VERBOSE=1 MYSQL_DSNS_JSON=t/tmp/dsns.json \
	$(PERL) bin/import.pl config/devel-sources.json

develdb-stop:
	$(PERL) $(PREPARE_DB_SET) --stop --dsn-list t/tmp/dsns.json

## ------ Server configuration ------

# Need SERVER_ENV!
server-config: daemontools-config batch-server

# Need SERVER_ENV!
install-server-config: install-daemontools-config

SERVER_ENV = HOGE
SERVER_ARGS = \
    APP_NAME=swantenna \
    SERVER_INSTANCE_NAME="swantenna-$(SERVER_ENV)" \
    SERVER_INSTANCE_CONFIG_DIR="$(abspath ./config)" \
    ROOT_DIR="$(abspath .)" \
    LOCAL_DIR="$(abspath ./local)" \
    LOG_DIR=/var/log/app \
    SYSCONFIG="/etc/sysconfig/suikawiki" \
    SERVICE_DIR="/service" \
    SERVER_USER=wakaba LOG_USER_GROUP=wakaba.wakaba \
    SERVER_ENV="$(SERVER_ENV)"

# Need SERVER_ENV!
daemontools-config:
	$(MAKE) --makefile=Makefile.service all $(SERVER_ARGS) SERVER_TYPE=web

# Need SERVER_ENV!
install-daemontools-config:
	mkdir -p /var/log/app
	chown wakaba.wakaba /var/log/app
	$(MAKE) --makefile=Makefile.service install $(SERVER_ARGS) SERVER_TYPE=web

# Need SERVER_ENV!
batch-server:
	mkdir -p local/config/cron.d
	cd config/cron.d.in && ls *-cron | \
            xargs -l1 -i% -- sh -c "cat % | sed 's/@@ROOT@@/$(subst /,\/,$(abspath .))/g' > ../../local/config/cron.d/$(SERVER_ENV)-%"

## ------ Tests ------

PROVE = ./prove

test: test-deps test-main

test-deps: deps

test-main:
	$(PROVE) t/*.t

## ------ Deployment ------

GIT = git
CINNAMON_GIT_REPOSITORY = https://github.com/wakaba/cinnamon.git

cinnamon:
	mkdir -p local
	cd local && (($(GIT) clone $(CINNAMON_GIT_REPOSITORY)) || (cd cinnamon && $(GIT) pull)) && cd cinnamon && $(MAKE) deps
	echo "#!/bin/sh" > ./cin
	echo "exec $(abspath local/cinnamon/perl) $(abspath local/cinnamon/bin/cinnamon) \"\$$@\"" >> ./cin
	chmod ugo+x ./cin
